class Taco {
    constructor(shell, seasoning, mixin, base_layer, condiment) {
        this.shell = shell;
        this.seasoning = seasoning;
        this.mixin = mixin;
        this.condiment = condiment;
        this.base_layer = base_layer;
    }

    createTacoHTML() {
        const elTaco = document.createElement('div');
        elTaco.className = 'taco';
        elTaco.appendChild( this.createSectionHTML( 'Shell', this.shell ) );
        elTaco.appendChild( this.createSectionHTML( 'Base layer', this.base_layer ) );
        elTaco.appendChild( this.createSectionHTML( 'Seasoning', this.seasoning ) );
        elTaco.appendChild( this.createSectionHTML( 'Mixin', this.mixin ) );
        elTaco.appendChild( this.createSectionHTML( 'Condiment', this.condiment ) );
        return elTaco;
    }

    createSectionHTML(section, { name, recipe }) {
        const elSection = document.createElement('section');
        elSection.className = 'taco-section'
        
        const elSectionTitle = document.createElement('h4');
        elSectionTitle.className = 'taco-section-title';
        elSectionTitle.innerText = `${section}: ${name}`;
        elSection.appendChild( elSectionTitle );

        const elSectionRecipe = document.createElement('p');
        elSectionRecipe.innerText = recipe;
        elSection.appendChild( elSectionRecipe );

        return elSection;
    }
}

export default Taco;