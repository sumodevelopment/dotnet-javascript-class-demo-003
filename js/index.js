import { getRandomName } from '../api/names.js';
import { getRandomTaco } from '../api/tacos.js';
import Taco from '../models/taco.js';

class App {

    constructor() {
        // HTML Elements
        this.elStatus = document.getElementById('status');
        this.elTacoName = document.getElementById('taco-name');
        this.elTaco = document.getElementById('taco');
        this.elBtnGetTaco = document.getElementById('btn-get-taco');

        // Properties
        this.error = '';
        this.tacoName = '';
        this.taco = null;

        return this;
    }

    async init() {
        this.bindEvents();
        await this.getTacoNewTaco();
        this.render();
    }

    bindEvents() {
        this.elBtnGetTaco.addEventListener('click', async () => {
            await this.getTacoNewTaco();
            this.render();
        });
    }

    async getTacoNewTaco() {
        try {
            this.elStatus.innerText = 'Loading a fresh new Taco 🌮 👩‍🍳';
           this.tacoName = await getRandomName(); 
           const { shell, seasoning, mixin, condiment, base_layer } = await getRandomTaco();
           this.taco = new Taco( shell, seasoning, mixin, base_layer, condiment );
        } catch (error) {
            this.error = error.message;
        } finally {
            this.elStatus.innerText = '';
        }
    }

    async handleNewRecipeClick() {
       await this.getTacoNewTaco();
       this.render();
    }

    resetDOM() {
        this.elTacoName.innerText = '';
        this.elTaco.innerHTML = '';
        this.elStatus.innerText = '';
    }

    render() {
        this.resetDOM();
        
        if (this.error) {
            this.elStatus.innerText = this.error;
            return;
        }

        // Render the name
        this.elTacoName.innerText = this.tacoName;
        // Render the recipe
        this.elTaco.appendChild( this.taco.createTacoHTML() );
    }
}

new App().init();