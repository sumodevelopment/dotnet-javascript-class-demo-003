const PROXY = 'https://cors-anywhere-noroff.herokuapp.com/';

export const getRandomName = () => {
    return fetch(`${PROXY}http://names.drycodes.com/1`)
            .then(r => r.json())
            .then(names => names.pop().replace(/_/g, ' '))
}

