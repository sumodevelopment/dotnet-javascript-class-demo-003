export const getRandomTaco = () => {
    return fetch('https://taco-randomizer.herokuapp.com/random/')
    .then(r => r.json())
}